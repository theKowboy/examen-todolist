@extends('layouts.app')

@section('content')

    <div class="container" xmlns:background-color="http://www.w3.org/1999/xhtml">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">New Tasks
                        <span class="pull-right">
                            <a href="#" id="task" data-toggle="modal" data-target="#modal_task" class="btn btn-default" aria-hidden="true">
                                <span class="glyphicon glyphicon-plus"  aria-hidden="true"></span>
                            </a>
                        </span>
                    </div>

                    <div class="panel-body" id="tasks">
                            @foreach($tasks as $i =>  $task)
                                @if($task->done == 0)
                                    <div class="list-group" id="{{ $task->id }}" value="{{ $task->id }}">
                                        <li class="list-group-item active taken" id="{{ $task->id }}" value="{{ $task->id }}" data-toggle="modal" data-target="#modal_task" >{{$task->name}}
                                            <input type="hidden" id="task_id" name="task_id" value="{{ $task->id }}"></li>
                                            <a data-toggle="modal" data-target="#modal_sub_task">
                                                <span class="glyphicon glyphicon-plus pull-right addSubTask" id="{{ $task->id }}" value="{{ $task->id }}" aria-hidden="false">Add</span>
                                            </a>
                                    </div>
                                    <div class="editSubTask">
                                        @foreach($task->subTasks as $subTasks)
                                            <a href="#" class="list-group-item editSubTask"  value="{{$subTasks->id}}"  >{{$subTasks->name}}
                                                <span class="glyphicon glyphicon-refresh pull-right editSubTask" id="editSubTask" value="{{$subTasks->id}}" aria-hidden="true"></span>
                                            </a>
                                        @endforeach
                                    </div>
                                @else
                                    <div class="list-group" id="{{ $task->id }}" style="background-color: #555555">
                                        <li class="list-group-item theTask" id="theTask" value="{{ $task->id }}"  style="background-color: #555555" >{{$task->name}}
                                            <input type="hidden" id="task_id" name="task_id" value="{{ $task->id }}">
                                        </li>
                                    </div>
                                @endif
                            @endforeach
                    </div>
                    <input type="hidden" id="block_id" name="block_id" value="{{ $blockId }}">

                </div>
            </div>
            <!-- Modal to create news Tasks -->
            <div class="modal fade" id="modal_task" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                            <h4 class="modal-title" id="title" name="title" >Add New Task</h4>
                        </div>

                        <div class="modal-body">
                            <input type="hidden" id="id">
                            <input type="text" placeholder="Write name to task here" name="name_task" id="name_task" class="form-control">
                            <input type="hidden" id="param-csrf" name="_token" value="{{ Crypt::encrypt(csrf_token()) }}">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" id="delete" data-dismiss="modal" style="display: none">Done</button>
                            <button type="button" class="btn btn-primary" id="save_changes" data-dismiss="modal" style="display: none">Save changes</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal" name="add_task" id="add_task">Add Task</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <!-- Modal to create news Sub Tasks -->
            <div class="modal fade" id="modal_sub_task" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                            <h4 class="modal-title" id="title" name="title" >New Sub Task</h4>
                        </div>

                        <div class="modal-body">
                            <input type="hidden" id="id">
                            <input type="text" placeholder="Write name to sub task here" name="name_sub_task" id="name_sub_task" class="form-control">
                            <input type="hidden" id="param-csrf" name="_token" value="{{ Crypt::encrypt(csrf_token()) }}">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" id="delete" data-dismiss="modal" style="display: none">Done</button>
                            <button type="button" class="btn btn-primary" id="save_changes" data-dismiss="modal" style="display: none">Save changes</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal" name="save" id="save">Save</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>
    </div>
    <script type="text/javascript" src="{{asset('js/subTask/subTask.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/task/task.js')}}"></script>


    <script type="text/javascript">
        // make routs for tasks
        global.createTasks = "{{route('createTasks')}}";
        global.updateTasks = "{{route('updateTasks')}}";
        global.deleteTasks = "{{route('deleteTasks')}}";

        // make routs for sub tasks
        global.createSubTasks = "{{route('createSubTasks')}}";
        global.updateSubTasks = "{{route('updateSubTasks')}}";
        global.deleteSubTasks = "{{route('deleteSubTasks')}}";

    </script>

@endsection
