@extends('layouts.app')

@section('content')

    <div class="container" xmlns:background-color="http://www.w3.org/1999/xhtml">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Blocks
                        <span class="pull-right">
                            <button type="button" data-toggle="modal" data-target="#modal_block" class="btn btn-default" aria-hidden="true">
                                <span class="glyphicon glyphicon-plus"  aria-hidden="true"></span>
                            </button>
                        </span>
                    </div>

                    <div class="panel-body"  id="blocks">
                        @if($blocks->count())
                            @foreach($blocks as $block)
                                @if($block->done == 0)
                                    <div class="list-group">
                                        <li id="theBlock" value="{{$block->id}}" class="list-group-item theBlock" data-toggle="modal" data-target="#modal_block">{{$block->blockName}}</li>
                                            <a href="{{  route('tasks',['id' => $block->id])}}" class="pull-right theBlock">
                                                <li class="glyphicon glyphicon-pencil theBlock" id="theBlock" >Tasks</li> </a>
                                    </div>
                                @else
                                    <div class="list-group" style="background-color: #555555">
                                        <li id="theBlock" value="{{$block->id}}" class="list-group-item theBlock" data-toggle="modal" data-target="#modal_block" style="background-color: #555555">{{$block->blockName}}</li>
                                    </div>
                                @endif
                            @endforeach
                        @else
                            <div class="list-group" >
                                <button type="button" class="list-group-item">Empty!</button>
                            </div>
                        @endif
                    </div>

                </div>
            </div>
            <!-- Modal to create news blocks -->
            <div class="modal fade" id="modal_block" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                            <h4 class="modal-title" id="title">Add New Block</h4>
                        </div>

                        <div class="modal-body">
                            <input type="hidden" id="id">
                            <p><input type="text" placeholder="Write name to block here" name="name_block" id="name_block" class="form-control"></p>
                            <input type="hidden" id="param-csrf" name="_token" value="{{ Crypt::encrypt(csrf_token()) }}">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" id="done" data-dismiss="modal" style="display: none">Done</button>
                            <button type="button" class="btn btn-default" id="delete" data-dismiss="modal" style="display: none">Delete</button>
                            <button type="button" class="btn btn-primary" id="save_changes" data-dismiss="modal" style="display: none">Save changes</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal" name="add_block" id="add_block">Add Block</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>
    </div>

    <script type="text/javascript" src="{{asset('js/block/block.js')}}"></script>
    <script type="text/javascript">
        global.createBlock = "{{route('create')}}";
        global.updateBlocks = "{{route('updateBlocks')}}";
        global.doneBlocks = "{{route('doneBlocks')}}";
        global.deleteBlocks = "{{route('deleteBlocks')}}";

    </script>

@endsection





