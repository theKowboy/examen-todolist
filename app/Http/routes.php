<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

// Routs for blocks
Route::get('/index', 'Block\BlockController@index');
Route::post('/create', 'Block\BlockController@create')->name('create');
Route::post('/update', 'Block\BlockController@update')->name('updateBlocks');
Route::post('/doneBlocks', 'Block\BlockController@doneBlocks')->name('doneBlocks');
Route::post('/deleteBlocks', 'Block\BlockController@deleteBlocks')->name('deleteBlocks');


// Routs for tasks
Route::get('/tasks/{id}', 'Task\TaskController@listTasks')->name('tasks');
Route::post('/createTasks', 'Task\TaskController@createTasks')->name('createTasks');
Route::post('/updateTasks', 'Task\TaskController@updateTasks')->name('updateTasks');
Route::post('/deleteTasks', 'Task\TaskController@deleteTasks')->name('deleteTasks');

// Routs for sub tasks
Route::post('/createSubTasks', 'SubTask\SubTaskController@createSubTasks')->name('createSubTasks');
Route::post('/updateSubTasks', 'SubTask\SubTaskController@updateSubTasks')->name('updateSubTasks');
Route::post('/deleteSubTasks', 'SubTask\SubTaskController@deleteSubTasks')->name('deleteSubTasks');