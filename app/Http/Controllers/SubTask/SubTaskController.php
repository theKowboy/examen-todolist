<?php

namespace App\Http\Controllers\SubTask;

use Illuminate\Http\Request;
use App\Models\SubTask;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SubTaskController extends Controller
{
    public function createSubTasks(Request $request)
    {
        $subTaskId = $request->get('subTaskId');

        $subTaskName = $request->get('name');
        $result = array();

        $subTask = SubTask::create([
            'name' => $subTaskName,
            'task_id' => $subTaskId
        ]);

        if (!empty($subTask)) {
            $result = ['data' => $subTask];
        } else {
            $result = response()->json([
                'message' => 'Cannot create this tasks, please try again later'
            ])->setStatusCode(500);
        }
        return response()->json($result);
    }


    public function updateSubTasks(Request $request)
    {
        $subTaskName = $request->get('name');
        $SubtaskId = $request->get('subTaskId');
        $result = SubTask::where('id', $SubtaskId)->update([
            'name' => $subTaskName,
        ]);

        return response()->json($result);
    }


    public function deleteSubTasks(Request $request)
    {
        $done = 1;
        $subTaskName = $request->get('name');
        $subTaskId = $request->get('subTaskId');

        $result = SubTask::where('id', $subTaskId)->update([
            'name' => $subTaskName,
            'done' => $done
        ]);

        return response()->json($result);
    }
}