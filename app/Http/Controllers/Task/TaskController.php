<?php

namespace App\Http\Controllers\Task;

use Illuminate\Http\Request;
use App\Models\Task;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Task\Log;

class TaskController extends Controller
{
    public function listTasks(Request $request)
    {
        $result = array();
        $blockId = $request->route('id');

        $tasks = Task::where('block_id', $blockId)->get();
        $lol = null;
        if (!$tasks->isEmpty()) {

            $tasks = $tasks->load('subTasks')->all();
        }

        return view('showTasks', ['tasks' => $tasks, 'blockId' => $blockId]);
    }

    public function createTasks(Request $request)
    {
        $blockId = $request->get('blockId');

        $taskName = $request->get('name');
        $result = array();

        $task  = Task::create([
            'name' => $taskName,
            'block_id' => $blockId
        ]);

        if (!empty($task) ) {
            $result = ['data' => $task];
        } else {
            $result = response()->json([
                'message' => 'Cannot create this tasks, please try again later'
            ])->setStatusCode(500);
        }
        return response()->json($result);
    }

    public function updateTasks(Request $request)
    {
        $blockId = $request->get('blockId');
        $taskName = $request->get('name');
        $taskId = $request->get('taskId');
        \Log::info($taskId);
        $result = Task::where('id', $taskId)->update([
            'name' => $taskName,
            'block_id' => $blockId
        ]);


        return response()->json($result);
    }

    public function deleteTasks(Request $request)
    {
        $done = 1;
        $taskName = $request->get('name');
        $taskId = $request->get('taskId');

        $result = Task::where('id', $taskId)->update([
            'name' => $taskName,
            'done' => $done
        ]);

        return response()->json($result);
    }

}
