<?php

namespace App\Http\Controllers\Block;

use App\Models\Block;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

//use Auth;

class BlockController extends Controller
{
    public function index()
    {
        $result = array();

        $blocks =  Block::where('user_id', Auth::id())
            ->select(
                'blocks.id AS id',
                'blocks.name AS blockName', 'blocks.done'
//                ,'tasks.name AS taskName',
//                'sub_tasks.name AS subTaskName'
            )
//            ->leftJoin('tasks', 'blocks.id', '=', 'tasks.block_id')
//            ->leftJoin('sub_tasks', 'tasks.id', '=', 'sub_tasks.task_id')
            ->get();

        if (!empty($blocks)) {
            # code...
            $result = ['blocks' => $blocks];
        } else {
            $result = ['message' => 'There are not blocks to show'];
        }
        return view('index', $result);

    }

    public function create(Request $request)
    {
        $name = $request->get('name');
        $result = array();

        $block  = Block::create([
            'name' => $name,
            'user_id' => auth()->user()->id
        ]);

        if (!empty($block) ) {
            $result = ['data' => $block];
        } else {
            $result = response()->json([
                'message' => 'Cannot create this block, please try again later'
            ])->setStatusCode(500);
        }
        return response()->json($result);
    }

    public function update(Request $request)
    {
        $blockName = $request->get('name');
        $blockId = $request->get('id');
        $result = Block::where('id', $blockId)->update([
            'name' => $blockName,
        ]);

        return response()->json($result);
    }

    public function doneBlocks(Request $request)
    {
        $done = 1;
        $blockId = $request->get('id');

        $result = Block::where('id', $blockId)->update([
            'done' => $done
        ]);

        return response()->json($result);
    }
    public function deleteBlocks(Request $request)
    {
        $blockId = $request->get('id');
        $result = Block::where('id', $blockId)->delete();

        return response()->json($result);
    }
}
