<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name',
        'block_id',
        'done'
    ];

    protected $dates = ['deleted_at'];

    public function block()
    {
        return $this->belongsTo(Block::class);
    }

    public function subTasks()
    {
        return $this->hasMany(SubTask ::class);
    }
}
