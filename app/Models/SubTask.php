<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubTask extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'task_id',
        'done'
    ];
    protected $dates = ['deleted_at'];

    public function task()
    {
        return $this->belongsTo(Task::class);
    }
}
