$(document).ready(function(){
    ifClickToEdit();
    ifClickToAdd();
    updateT();
    doneT();
    createT();
});

function createT() {
    $('#add_task').click(function(event){
        var nameTask = $('#name_task').val();
        var blockId = $('input[name=block_id]').val();
        var token = $('input[name=_token]').val();
        if (nameTask == ""){
            alert('plase typing a valid name')
        }else {
            $.ajax({
                method: "POST",
                url: global.createTasks,
                dataType: "json",
                data: {'name': nameTask, 'blockId': blockId},

                success: function success(response) {
                    $("#tasks").load(location.href + ' #tasks');
                },
                error: function error(errors) {
                    alert('Cannt create this task please try again later')
                },
                beforeSend: function beforeSend(xhr) {
                    return xhr.setRequestHeader('X-XSRF-TOKEN', token);
                }
            });
        }
    });
}

function ifClickToEdit() {
    $(document).on('click', '.taken', function (event) {
        var taskId = $(this).val();
        var text = $(this).text();
        $('#title').text("Edit Task");
        var text = $.trim(text);
        $('#name_task').val(text);
        $('#delete').show('400');
        $('#save_changes').show('400');
        $('#add_task').hide('400');
        $('#id').val(taskId);
    });
}
function myFunction(event) {
    return event.target.id;
}
function ifClickToAdd() {
    $(document).on('click', '#task', function(event) {
        event.preventDefault();
        ///alert('if click to add ');
        $('#title').text("Add Task");
        $('#name_task').val('');
        $('#delete').hide('400');
        $('#save_changes').hide('400');
        $('#add_task').show('400');
    });
}


function updateT() {
    $('#save_changes').click(function(event){
    //$(document).on('click', '#save_changes', function(event) {
            var nameTask = $('#name_task').val();
            var blockId = $('input[name=block_id]').val();
            var taskId = $('#id').val();
            var token = $('input[name=_token]').val();

            $.ajax({
                method: "POST",
                url: global.updateTasks,
                dataType: "json",
                data: {'name': nameTask, 'blockId': blockId, 'taskId': taskId},

                success: function success(response) {
                    $("#tasks").load(location.href + ' #tasks');
                },
                error: function error(errors) {
                    alert('Cannt create this task please try again later')
                },
                beforeSend: function beforeSend(xhr) {
                    return xhr.setRequestHeader('X-XSRF-TOKEN', token);
                }
            });
            $('#name_task').text('');
        });
}

function doneT() {
    $('#delete').click(function(event){
        var nameTask = $('#name_task').val();
        var blockId = $('input[name=block_id]').val();
        var taskId = $('#id').val();
        var token = $('input[name=_token]').val();

        $.ajax({
            method: "POST",
            url: global.deleteTasks,
            dataType: "json",
            data: {'name': nameTask, 'blockId': blockId, 'taskId': taskId},

            success: function success(response) {
                $("#tasks").load(location.href + ' #tasks');
            },
            error: function error(errors) {
                alert('Cannt Delete this task please try again later')
            },
            beforeSend: function beforeSend(xhr) {
                return xhr.setRequestHeader('X-XSRF-TOKEN', token);
            }
        });
    });
}
