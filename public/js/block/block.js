$(document).ready(function(){
    deleteB();
    create();
    ifClickToEdit();
    updateB();
    doneB();
});

function create() {
    $('#add_block').click(function(event){
        var nameBlock = $('#name_block').val();
        var token = $('input[name=_token]').val();
        if (nameBlock == ''){
            alert('Please write a valid name')
        }else{
            $.ajax({
                method: "POST",
                url: global.createBlock,
                dataType: "json",
                data: {'name': nameBlock},

                success: function success(response) {
                    $("#blocks").load(location.href + ' #blocks');
                },
                error: function error(errors) {
                   alert('Error!')
                },
                beforeSend: function beforeSend(xhr) {
                    return xhr.setRequestHeader('X-XSRF-TOKEN', token);
                }
            });
            $('#name_block').val('');
        }
    });
}
function ifClickToEdit() {
    $(document).on('click', '.theBlock', function (event) {
        var text = $(this).text();
        var blockId = $(this).val();
        $('#title').text("Edit Block");
        var text = $.trim(text);
        $('#name_block').val(text);
        $('#done').show('400');
        $('#delete').show('400');
        $('#save_changes').show('400');
        $('#add_block').hide('400');
        $('#id').val(blockId);
    });
}

function updateB() {
    $('#save_changes').click(function(event){
        var nameBlock = $('#name_block').val();
        var token = $('input[name=_token]').val();
        var blockId = $('#id').val();
        if (nameBlock == ''){
            alert('Please write a valid name')
        }else {
            $.ajax({
                method: "POST",
                url: global.updateBlocks,
                dataType: "json",
                data: {'name': nameBlock, 'id': blockId},

                success: function success(response) {
                    $("#blocks").load(location.href + ' #blocks');
                },
                error: function error(errors) {
                    alert('Cannt create this task please try again later')
                },
                beforeSend: function beforeSend(xhr) {
                    return xhr.setRequestHeader('X-XSRF-TOKEN', token);
                }
            });
            $('#name_task').text('');
        }
    });
}

function deleteB() {
    $('#delete').click(function(event){
        var blockId = $('#id').val();
        var token = $('input[name=_token]').val();

        $.ajax({
            method: "POST",
            url: global.deleteBlocks,
            dataType: "json",
            data: { 'id': blockId},

            success: function success(response) {
                $("#blocks").load(location.href + ' #blocks');
            },
            error: function error(errors) {
                alert('Cannot delete this task please try again later')
            },
            beforeSend: function beforeSend(xhr) {
                return xhr.setRequestHeader('X-XSRF-TOKEN', token);
            }
        });
    });
}

function doneB() {
    $('#done').click(function(event){
        var blockId = $('#id').val();
        var token = $('input[name=_token]').val();
        $.ajax({
            method: "POST",
            url: global.doneBlocks,
            dataType: "json",
            data: {'id': blockId},

            success: function success(response) {
                $("#blocks").load(location.href + ' #blocks');
            },
            error: function error(errors) {
                alert('Cannt Delete this task please try again later')
            },
            beforeSend: function beforeSend(xhr) {
                return xhr.setRequestHeader('X-XSRF-TOKEN', token);
            }
        });

    });
}
